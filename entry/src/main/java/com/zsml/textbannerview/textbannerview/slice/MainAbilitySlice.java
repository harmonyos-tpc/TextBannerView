package com.zsml.textbannerview.textbannerview.slice;

import com.superluo.textbannerlibrary.ITextBannerItemClickListener;
import com.superluo.textbannerlibrary.TextBannerView;
import com.superluo.textbannerlibrary.utils.ToastUtils;
import com.zsml.textbannerview.textbannerview.MainAbility;
import com.zsml.textbannerview.textbannerview.ResUtil;
import com.zsml.textbannerview.textbannerview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.window.service.WindowManager;
import ohos.media.image.PixelMap;

import java.util.ArrayList;
import java.util.List;

import static ohos.agp.utils.LayoutAlignment.LEFT;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
       // StatusBarUtil.immersive(this);
        initView();
        initData();
        setListener();
    }



    @Override
    public void onActive() {
        super.onActive();
        /**调用startViewAnimator()重新开始文字轮播*/
        mTvBanner.startViewAnimator();
        mTvBanner1.startViewAnimator();
        mTvBanner2.startViewAnimator();
        mTvBanner3.startViewAnimator();
        mTvBanner4.startViewAnimator();
    }


    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }





    private TextBannerView mTvBanner;
    private TextBannerView mTvBanner1;
    private TextBannerView mTvBanner2;
    private TextBannerView mTvBanner3;
    private TextBannerView mTvBanner4;
    private List<String> mList;




    private void initView() {

        mTvBanner = (TextBannerView) findComponentById(ResourceTable.Id_tv_banner);
        mTvBanner1 = (TextBannerView) findComponentById(ResourceTable.Id_tv_banner1);
        mTvBanner2 = (TextBannerView) findComponentById(ResourceTable.Id_tv_banner2);
        mTvBanner3 = (TextBannerView) findComponentById(ResourceTable.Id_tv_banner3);
        mTvBanner4 = (TextBannerView) findComponentById(ResourceTable.Id_tv_banner4);
    }

    private void initData() {
        mList = new ArrayList<>();
        mList.add("学好Java、Android、C#、C、ios、html+css+js");
        mList.add("走遍天下都不怕！！！！！");
        mList.add("不是我吹，就怕你做不到，哈哈");
        mList.add("superluo");
        mList.add("你是最棒的，奔跑吧孩子！");
        /**
         * 设置数据，方式一
         */
        mTvBanner.setDatas(mList);
        mTvBanner.setDatas(mList);
        mTvBanner1.setDatas(mList);
        mTvBanner2.setDatas(mList);
        mTvBanner3.setDatas(mList);


        PixelMap drawableMap = ResUtil.getPixelMap(getContext(), ResourceTable.Media_ic_launcher).get();
        PixelMapElement drawable = new PixelMapElement(drawableMap);
        /**
         * 设置数据（带图标的数据），方式二
         */
        //第一个参数：数据 。第二参数：drawable.  第三参数drawable尺寸。第四参数图标位置
        mTvBanner4.setDatasWithDrawableIcon(mList,drawable,18, LEFT);

    }

    private void setListener() {
        mTvBanner.setItemOnClickListener(new ITextBannerItemClickListener() {
            @Override
            public void onItemClick(String data, int position) {
                ToastUtils.makeText(getContext(), String.valueOf(position)+">>"+data);
            }
        });

        mTvBanner1.setItemOnClickListener(new ITextBannerItemClickListener() {
            @Override
            public void onItemClick(String data, int position) {
                ToastUtils.makeText(getContext(), String.valueOf(position)+">>"+data);
            }
        });

        mTvBanner2.setItemOnClickListener(new ITextBannerItemClickListener() {
            @Override
            public void onItemClick(String data, int position) {
                ToastUtils.makeText(getContext(), String.valueOf(position)+">>"+data);
            }
        });

        mTvBanner3.setItemOnClickListener(new ITextBannerItemClickListener() {
            @Override
            public void onItemClick(String data, int position) {
                ToastUtils.makeText(getContext(), String.valueOf(position)+">>"+data);
            }
        });

        mTvBanner4.setItemOnClickListener(new ITextBannerItemClickListener() {
            @Override
            public void onItemClick(String data, int position) {
                ToastUtils.makeText(getContext(), String.valueOf(position)+">>"+data);
            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
        /**调用stopViewAnimator()暂停文字轮播，避免文字重影*/
        mTvBanner.stopViewAnimator();
        mTvBanner1.stopViewAnimator();
        mTvBanner2.stopViewAnimator();
        mTvBanner3.stopViewAnimator();
        mTvBanner4.stopViewAnimator();
    }




}

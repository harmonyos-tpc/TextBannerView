package com.superluo.textbannerlibrary.utils;

import java.util.List;

/**
 * Display utils
 */
public class DisplayUtils {

    /**
     * Not empty boolean
     *
     * @param <T>  parameter
     * @param list list
     * @return the boolean
     */
    public static <T> boolean notEmpty(List<T> list) {
        return !isEmpty(list);
    }

    /**
     * Is empty boolean
     *
     * @param <T>  parameter
     * @param list list
     * @return the boolean
     */
    public static <T> boolean isEmpty(List<T> list) {
        if (list == null || list.size() == 0) {
            return true;
        }
        return false;
    }

    /**
     * Px 2 sp int
     *
     * @param pxValue px value
     * @return the int
     */
    public static int px2sp(float pxValue) {
        return (int) (pxValue * 3);
    }






}